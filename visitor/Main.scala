import org.antlr.v4.runtime._
import org.antlr.v4.runtime.tree._
import java.io.FileInputStream
import java.io.InputStream

object Main {
  def main(args: Array[String]): Unit = {
    var istream = args match {
      case Array(inputfile) => new FileInputStream(inputfile)
      case _ => System.in
    }

		var input  = CharStreams.fromStream(istream)
		var lexer  = new ExprLexer(input)
		var tokens = new CommonTokenStream(lexer)
		var parser = new ExprParser(tokens)
		var tree   = parser.prog()
		println(tree.toStringTree(parser))

		var eval = new EvalVisitor()
		eval.visit(tree)
  }
}
