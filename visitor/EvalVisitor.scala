import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor
import scala.collection.mutable.Map
import ExprParser._

class EvalVisitor extends ExprBaseVisitor[Int] {
  private var vars = Map[String, Int]()

  override def visitParens(ctx: ParensContext): Int = visit(ctx.expr)

  override def visitId(ctx: IdContext): Int = vars.getOrElse(ctx.ID.getText, 0)

  override def visitPrintExpr(ctx: PrintExprContext): Int = {
    println(visit(ctx.expr))
    0
  }

  override def visitAssignment(ctx: AssignmentContext): Int = {
    val (id, v) = (ctx.ID.getText, visit(ctx.expr))
    vars(id) = v
    v
  }

  override def visitAddsub(ctx: AddsubContext): Int = {
    val (lhs, rhs) = (visit(ctx.expr(0)), visit(ctx.expr(1)))
    ctx.op.getType match {
      case ADD => lhs + rhs
      case SUB => lhs - rhs
      case _ => 0
    }
  }

  override def visitMuldiv(ctx: MuldivContext): Int = {
    val (lhs, rhs) = (visit(ctx.expr(0)), visit(ctx.expr(1)))
    ctx.op.getType match {
      case MUL => lhs * rhs
      case DIV => lhs / rhs
      case _ => 0
    }
  }

  override def visitInt(ctx: IntContext) =
    Integer.valueOf(ctx.INT_LITERAL.getText)
}
