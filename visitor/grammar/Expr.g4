grammar Expr;

// 使用 import 来完成语法的模块化
import CommonLexerRules;

prog
	: stat+
	;

// 使用 # 来给 rule 做标记
stat
	: expr NEWLINE					# printExpr
	| ID '=' expr NEWLINE		# assignment
	| NEWLINE								# blank
	;

expr
	: expr op=(MUL|DIV) expr # muldiv
	| expr op=(ADD|SUB) expr # addsub
	| INT_LITERAL            # int
	| ID                     # id
	| '(' expr ')'           # parens
	;

MUL: '*';
DIV: '/';
ADD: '+';
SUB: '-';
