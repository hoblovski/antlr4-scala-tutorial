import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor
import scala.collection.mutable.Map
import ExprParser._

class EvalVisitor extends ExprBaseVisitor[Int] {
  private var vars = Map[String, Int]()

  override def visitParens(ctx: ParensContext) = visit(ctx.expr)

  override def visitId(ctx: IdContext) = vars.getOrElse(ctx.ID.getText, 0)

  override def visitPrintExpr(ctx: PrintExprContext) = {
    println(visit(ctx.expr))
    0
  }

  override def visitAssignment(ctx: AssignmentContext) = {
    val (id, v) = (ctx.ID.getText, visit(ctx.expr))
    vars(id) = v
    v
  }

  override def visitBinop(ctx: BinopContext) = {
    val (lhs, rhs) = (visit(ctx.lhs), visit(ctx.rhs))
    ctx.op.getType match {
      case ADD => lhs + rhs
      case SUB => lhs - rhs
      case MUL => lhs * rhs
      case DIV => lhs / rhs
      case _ => 0
    }
  }

  override def visitInt(ctx: IntContext) =
    Integer.valueOf(ctx.INT_LITERAL.getText)
}
