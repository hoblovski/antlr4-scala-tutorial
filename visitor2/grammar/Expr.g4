grammar Expr;

// 使用 import 来完成语法的模块化
import CommonLexerRules;

prog
	: stat+
	;

// 使用 # 来给 rule 做标记
stat
	: expr NEWLINE					# printExpr
	| ID '=' expr NEWLINE		# assignment
	| NEWLINE								# blank
	;

// 靠前的规则优先
expr
	: lhs=expr op=(MUL|DIV) rhs=expr # binop
	| lhs=expr op=(ADD|SUB) rhs=expr # binop
	| INT_LITERAL            # int
	| ID                     # id
	| '(' expr ')'           # parens
	;

MUL: '*';
DIV: '/';
ADD: '+';
SUB: '-';
