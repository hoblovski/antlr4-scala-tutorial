import scala.collection.mutable.{ArrayStack => Stack, Map}
import ExprParser._

class EvalListener extends ExprBaseListener {
  var stack = Stack[Int]()
  var vars = Map[String, Int]()

  override def exitInt(ctx: IntContext): Unit = {
    stack push Integer.valueOf(ctx.INT_LITERAL.getText)
  }

  override def exitId(ctx: IdContext): Unit = {
    stack push vars.getOrElse(ctx.ID.getText, 0)
  }

  override def exitAddsub(ctx: AddsubContext): Unit = {
    require(stack.size >= 2)
    val rhs = stack.pop
    val lhs = stack.pop
    stack push (ctx.op.getType match {
      case ADD => lhs + rhs
      case SUB => lhs - rhs
      case _ => 0
    })
  } ensuring(x => stack.size >= 1)

  override def exitMuldiv(ctx: MuldivContext): Unit = {
    require(stack.size >= 2)
    val rhs = stack.pop
    val lhs = stack.pop
    stack push (ctx.op.getType match {
      case MUL => lhs * rhs
      case DIV => lhs / rhs
      case _ => 0
    })
  } ensuring (x => stack.size >= 1)

  override def exitAssignment(ctx: AssignmentContext): Unit = {
    require(stack.size >= 1)
    val (id, v) = (ctx.ID.getText, stack.pop)
    vars(id) = v
  }

  override def exitPrintExpr(ctx: PrintExprContext): Unit = {
    println(stack.pop)
  } ensuring(x => stack.isEmpty)
}
