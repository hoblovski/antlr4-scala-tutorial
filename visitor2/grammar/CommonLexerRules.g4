lexer grammar CommonLexerRules;

ID
	: [a-zA-Z]+
	;

INT_LITERAL
	: [0-9]+
	;

NEWLINE
	: '\r'? '\n'
	;

WS
	: [ \t] -> skip
	;

