import scala.collection.mutable.{ArrayStack => Stack, Map}
import ExprParser._
import org.antlr.v4.runtime.tree._

class EvalListener extends ExprBaseListener {
  var stack = Stack[Int]()
  var vars = Map[String, Int]()
  var values = new ParseTreeProperty[Int]()

  override def exitInt(ctx: IntContext): Unit = {
    val intval = Integer.valueOf(ctx.INT_LITERAL.getText)
    values put (ctx, intval)
  }

  override def exitId(ctx: IdContext): Unit = {
    val idval = vars.getOrElse(ctx.ID.getText, 0)
    values put (ctx, idval)
  }

  override def exitAddsub(ctx: AddsubContext): Unit = {
    val (lhs, rhs) = (values get ctx.expr(0), values get ctx.expr(1))
    values put (ctx, ctx.op.getType match {
      case ADD => lhs + rhs
      case SUB => lhs - rhs
    })
  }

  override def exitMuldiv(ctx: MuldivContext): Unit = {
    val (lhs, rhs) = (values get ctx.expr(0), values get ctx.expr(1))
    values put (ctx, ctx.op.getType match {
      case MUL => lhs * rhs
      case DIV => lhs / rhs
    })
  }

  override def exitAssignment(ctx: AssignmentContext): Unit = {
    val (id, v) = (ctx.ID.getText, values get ctx.expr)
    vars(id) = v
  }

  override def exitParens(ctx: ParensContext): Unit = {
    values put (ctx, values get ctx.expr)
  }

  override def exitPrintExpr(ctx: PrintExprContext): Unit = {
    println(values get ctx.expr)
  }
}
